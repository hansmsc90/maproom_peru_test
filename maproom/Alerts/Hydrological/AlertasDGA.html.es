<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Alerts_Discharge" />
<title>Water Level Alerts</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="es" href="AlertasDGA.html?Set-Language=en" />
<link class="share" rel="canonical" href="AlertasDGA.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Alerts_Hydrological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/a:/.Hourly/a:/.lon/:a:/.lat/2/copy/:a:/.water_level/:a%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/:a:/.Alert_levels/:a/BLUE/0.0/mul//NO_ALERT/add_variable/%7BNO_ALERT/BLUE/YELLOW/RED%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add%5BM%5Ddominant_class//long_name/%28Water%20Level%20Alert%29def/startcolormap/transparent/green/green/blue/yellow/red/red/endcolormap/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig/T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef//plotborder+0+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform">
         <input class="carryup carry " name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="carry dlimgts dlimgloc share" name="region" type="hidden" />
         <input class="pickarea admin" name="resolution" type="hidden" data-default="irids:SOURCES:Chile:DGA:hydrological:Current:Hourly:ds" />

</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Alerts/">Alertas Agroclim&#225;ticas</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Alerts_Hydrological_term"><span property="term:label">Alertas Hidrol&#243;gicas</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>
<!--            <fieldset class="navitem">
		<legend>Variable</legend>
	<span class="selectvalue"></span><select class="pageformcopy" name="anal"><option value="">Anomalia</option><option value="Observed">Observado</option></select></fieldset>
-->

<fieldset class="navitem"><legend>Selección de Estación</legend>	
    <link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert//labelgeoIdintersect/%7Bndim/0.0/gt/%7B%28bb:-180:-90:180:90:bb%29geoobject%7Dif/2//labelgeoIdintersect/publicproc:/%7Bageom/ageoobject%7Dinputs/ageom/dup/parent/.dataset/.label/{location/label}ds/location/ageoobject/geometryintersects/0/maskle/SELECT/location/label/1/index/.streamgrids/iridl:geoId/(:ds)rsearch/{pop/pop}if/1/index/a:/.datatype//namearraytype/eq/:a:/.name/:a/cvntos/(:)exch/append/exch/{(%40%25s:ds)}{(%40%25d:ds)}ifelse/append/append/sprintf//name//region/def/use_as_grid/nip/nip/1output/:publicproc%7Ddef/%28irids:SOURCES:Chile:DGA:hydrological:Current:Hourly:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersect/info.json" />
          <select class="pageformcopy" name="region">
            <optgroup class="template" label="Label">
            <option class="iridl:values region@value label"></option>
              </optgroup>
            </select>
</fieldset>

</div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
    </ul>

<fieldset class="regionwithinbbox dlimage" about="">

<div style="float: left;">
<img class="dlimgloc" src="http://www.climatedatalibrary.cl/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-80%2C-56%2C-60%2C-16%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
</div>
<div style="float: left;">
<div class="valid" style="text-align: top;">
<a class="dlimgloc" rel="iridl:hasJSON"
href="http://www.climatedatalibrary.cl/expert/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/info.json" ></a>
<div class="template" style="color : black">Estaci&#243;n DGA <b><span class="iridl:long_name"></span></b>
</div>
</div>
<div class="valid" style="text-align: top;">
<a class="station" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/nombre/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json"></a>
<script type="application/json" property="iridl:hasPUREdirective">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
<div><table class="valid template">
<tr style="color : black"><td class="name "></td><td align="right" class="value "></td></tr>
</table>
</div>
</div>
</div>
<div class="dlimgtsbox">
<img class="dlimgts" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/Hourly/water_level/T/last/dup/24.0/7.0/mul/sub/exch/RANGE/nombre/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE/0.0/100.0/masknotrange//fullname/%28past%207%20days%20%28m%29%29/def/Alert_levels/nombre//region/get_parameter/geoobject/.nombre/.gridvalues/VALUE/a%3A/.RED/T/0.0/mul/add/DATA/0/AUTO/RANGE//fullname/%28RED%20alert%29/def/%3Aa%3A/.YELLOW/T/0.0/mul/add//fullname/%28YELLOW%20alert%29/def/%3Aa%3A/.BLUE/T/0.0/mul/add//fullname/%28BLUE%20alert%29/def/%3Aa/4/-1/roll/T/fig-/solid/thinnish/red/line/yellow/line/blue/line/medium/black/line/-fig/+.gif" />
</div>
</fieldset>

<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/a%3A/.Hourly/a%3A/.lon/%3Aa%3A/.lat/2/copy/%3Aa%3A/.water_level/%3Aa/%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/%3Aa%3A/.Alert_levels/%3Aa/BLUE/0.0/mul//NO_ALERT/add_variable/%7BNO_ALERT/BLUE/YELLOW/RED%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add/%5BM%5Ddominant_class//long_name/%28Water%20Level%20Alert%29/def/T/last/dup/240.0/sub/exch/RANGE/startcolormap/transparent/green/green/blue/yellow/red/red/endcolormap/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef/T/last/plotvalue/X/-80.04166/-64.95834/plotrange/Y/-56/-16/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/#options" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/a:/.Hourly/a:/.lon/:a:/.lat/2/copy/:a:/.water_level/:a/%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/:a:/.Alert_levels/:a/BLUE/0.0/mul//NO_ALERT/add_variable/%7BNO_ALERT/BLUE/YELLOW/RED%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add/%5BM%5Ddominant_class//long_name/%28Water%20Level%20Alert%29/def/T/last/dup/240.0/sub/exch/RANGE/startcolormap/transparent/green/green/blue/yellow/red/red/endcolormap/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef/T/last/plotvalue/X/-80.04166/-64.95834/plotrange/Y/-56/-16/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/+//T/last/plotvalue//plotaxislength+432+psdef//XOVY+null+psdef//plotborder+72+psdef+.gif" border="0" alt="image" />
<img class="dlauximg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/a:/.Hourly/a:/.lon/:a:/.lat/2/copy/:a:/.water_level/:a/%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/:a:/.Alert_levels/:a/BLUE/0.0/mul//NO_ALERT/add_variable/%7BNO_ALERT/BLUE/YELLOW/RED%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add/%5BM%5Ddominant_class//long_name/%28Water%20Level%20Alert%29/def/T/last/dup/240.0/sub/exch/RANGE/startcolormap/transparent/green/green/blue/yellow/red/red/endcolormap/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef//fntsze/20/psdef/T/last/plotvalue/X/-80.04166/-64.95834/plotrange/Y/-56/-16/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/.auxfig+//T/last/plotvalue//plotaxislength+432+psdef//XOVY+null+psdef//plotborder+72+psdef+.gif" />
</fieldset>

<fieldset class="regionwithinbbox">

<b>Nivvel de agua el las &#250;ltimas 24h</b>
<br />

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>Stations</td><td>-23h</td></tr>
<tr class="iridl:values"><td class="nombre"></td>
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/second/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-22h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/2./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-21h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/3./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-20h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/4./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-19h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/5./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-18h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/6./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-17h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/7./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-16h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/8./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-15h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/9./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-14h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/10./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-13h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/11./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-12h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/12./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-11h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/13./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-10h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/14./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-9h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/15./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-8h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/16./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-7h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/17./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-6h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/18./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-5h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/19./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-4h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/20./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-3h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/21./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-2h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/first/22./add/VALUE/toNaN/info.json" />
<table class="valid template">
<tr><td>-1h</td></tr>
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>

<div class="admin" style="display: inline-block; text-align: top">
<div>
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/last/VALUE/T//pointwidth/0/def/pop/nombre/first/VALUE/nombre/removeGRID/info.json" />
<script type="application/json" property="iridl:hasPUREdirective">
{"tr": {
 "var&lt;-iridl:hasIndependentVariables": {
 "+td.value": "var.iridl:value"
}
}
}
</script>
<table class="valid template">
<tr style="color : black"><td align="right" class="value "></td></tr>
</table>
</div>

<div>
<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/water_level/T/last/dup/23.0/sub/exch/RANGE%5BT/nombre%5DREORDER/CopyStream/location/%28bb:-72:-35:-69:-32:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/T/last/VALUE/toNaN/info.json" />
<table class="valid template">
<tr class="iridl:values">
<td><span class="water_level"></span>m</td>
</tr></table>
</div>
</div>

</fieldset>

 <div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Alerta de Niveles de Agua</h2>
<p align="left" property="term:description">Informaci&#243;n sobre las alertas de los niveles de agua en los r&#237;os de Chile estan disponibles aqu&#237;.</p>
<!-- <p align="left">In menu>variable you can select the variable: measured and anomaly. In menu>region you can select a region.</p>
<p align="left"><b>Measured:</b>
This map shows the discharge of the principal Chilean rivers. The measurements are in m<sup> 3 </sup>/s.</p>
<p align="left"><b>Anomaly:</b>
This map shows the discharge as anomaly, which indicates conditions of deficit or surplus compared to a normal situation. The anomaly is the difference between the discharge measured and the discharge normally observed. </p>
<p align="left">Discharge stations are those reported by the Chilean Water Authority (DGA).</p>
-->

</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h2  align="center">M&#225;s informaci&#243;n</h2>
<p> </p>
</div>
<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">Dataset Documentation</h2>
<h4><a class="carry">Niveles de agua</a></h4>
<dl class="datasetdocumentation">
<dt>Data</dt><dd>Nivel de agua en estaciones de la DGA</dd>
<dt>Data Source</dt><dd>Direcci&#243;n General de Aguas, <a href="http://www.dga.cl/"
>DGA</a></dd>
</dl>
</div>
<div class="ui-tabs-panel-hidden"> 
<h2 align="center">Fuente de los Datos</h2>
<p> <a href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/">Accede a la base de datos usado para crear este mapa.</a> </p>
</div>
<div id="tabs-4"  class="ui-tabs-panel">
<h2  align="center">Soporte</h2>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
</div>
 </body>
 </html>

