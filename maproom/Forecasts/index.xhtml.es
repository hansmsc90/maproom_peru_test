<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Pronosticos Estacionales</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<meta property="maproom:Sort_Id" content="a04" />
<link class="" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Forecast%20for%20%25=%5Btarget_date%5D%29/psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82.0/-68.0/plotrange/Y/-18.3/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29/psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef//L/1.0/plotvalue//F/last/plotvalue//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef+//plotaxislength+432+psdef//plotborder+0+psdef//antialias+true+psdef//color_smoothing+1+psdef//framelabel+%28Forecast%20for%20%25=%5Btarget_date%5D%29+psdef//framelabelstart+%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29+psdef+.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Peru</legend> 
                    <a rev="section" class="navlink carryup" href="/maproom/">Observatorio de Sequ&#237;as</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Peru</legend> 
                     <span class="navtext">Predicciones Estacionales</span>
            </fieldset> 
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Pronosticos Estacionales</h2>
<p align="left" property="term:description">
Pronsticos estacionales pueden ser instrumentos relevantes para preparar riesgos climaticos futuros. En este 'maproom' se visualiza los pronosticos de modelos internacionales y pronosticos con mayor detalle local</p><p>Pronosticos fueron realizados usando el IRI Climate Prediction Tool (CPT).
</p>
</div>
<div class="rightcol tabbedentries" about="/maproom/Forecasts/" >
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
</div>
 </body>

 </html>
