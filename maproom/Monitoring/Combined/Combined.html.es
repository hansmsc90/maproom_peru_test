<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Indice de Sequ&#237;a Combinado</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="Combined.html?Set-Language=en" />
<link class="share" rel="canonical" href="Combined.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Combined_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.CDI/CDI_complete/mask_CDI/mul/nip//long_name/%28Indice%20Combinado%20de%20Sequias%29def/X/Y/fig-/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//aprod/0/10/plotrange/X/-75.00999/-69.0/plotrange/Y/-44.99333/-28.99333/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//T/last/plotvalue/Y/-44.99333/-28.99333/plotrange//plotborder/72/psdef//plotaxislength/432/psdef//XOVY/null/psdef/++//T/last/plotvalue/Y/-44.99333/-28.99333/plotrange+//plotaxislength+432+psdef//plotborder+72+psdef//XOVY+null+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg share" name="bbox" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="dlimg dlauximg share" name="anal" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Combined_term"><span property="term:label">Indice Sequia Combinado</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
           </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>
<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.CDI/CDI_complete/mask_CDI/mul/nip//long_name/%28Indice%20Combinado%20de%20Sequias%29def/X/Y/fig-/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//aprod/0/10/plotrange/X/-75.00999/-69.0/plotrange/Y/-44.99333/-28.99333/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//T/last/plotvalue/Y/-44.99333/-28.99333/plotrange//plotborder/72/psdef//plotaxislength/432/psdef//XOVY/null/psdef/" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.CDI/CDI_complete/mask_CDI/mul/nip//long_name/%28Indice%20Combinado%20de%20Sequias%29def/X/Y/fig-/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//aprod/0/10/plotrange/X/-75.00999/-69.0/plotrange/Y/-44.99333/-28.99333/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//T/last/plotvalue/Y/-44.99333/-28.99333/plotrange//plotborder/72/psdef//plotaxislength/432/psdef//XOVY/null/psdef/++//T/last/plotvalue/Y/-44.99333/-28.99333/plotrange+//plotaxislength+432+psdef//plotborder+72+psdef//XOVY+null+psdef+.gif"  border="0" alt="image" /> <br />
<img class="dlauximg" src="Colorscale_esp.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h3 align="center"  property="term:title" >&#205;ndice de Sequ&#237;a Combinado</h3>
<p align="left" property="term:description">Este mapa muestra el &#205;ndice de Sequia Combinado (CDI), el cual informa sobre la condici&#243;n actual de sequ&#237;a en Chile.</p>
<p align="left">El &#205;ndice de Sequ&#237;a Combinado combina indicadores de la sequ&#237;a meteorol&#243;gica (&#205;ndice de Precipitaci&#243;n Estandarizado, IPE), la sequ&#237;a agr&#237;cola (FAPAR) e informaci&#243;n de la humedad de suelo.</p> 
<p align="left">El CDI considera tres niveles de impacto cada uno con diferentes niveles de intensidad:</p>
<p align="left"><h6>Tabla 1: Niveles de impacto del CDI</h6></p>
<p align="left"><img src="CDI.esp.png" alt="Combined"> </img></p>
</div>
<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;C&#243;mo se determina los tres niveles de impacto?</h3>
<p>
Dependiendo los valores del IPE, humedad de suelo y anomal&#237;a del FAPAR las observaciones del CDI son clasificados en los tres niveles de impacto y nivel de intensidad. La siguiente tabla define los criterios de clasificaci&#243;n:</p>
<p align="left"><h6>Tabla 1: Criterios de classificaci&#243;n de los niveles de impacto del CDI </h6></p>
<p align="left"><img src="CDI.esp.png" alt="Combined"> </img></p>
</div>
<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Fuente de los Datos</h3>
<p align="left"><a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.CDI/.CDI_complete/">&#205;ndice de Sequ&#237;a Combinado</a>, calculado con el &#205;ndice de Precipitaci&#243;n Estandardizado <a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.DMC/.SPI/">(IPE)</a>, el &#205;ndice de la Diferencia de Agua en la vegetaci&#243;n Normalizada <a href="http://www.climatedatalibrary.cl/SOURCES/.VITO/.NDWI_layers/.Combined/">(NDWI)</a>, y la Fracci&#243;n de la Radiaci&#243;n Fotosint&#233;ticamente Activo Absorbida <a href="http://www.climatedatalibrary.cl/SOURCES/.VITO/.FAPAR_layers/.Combined/.FAPAR_Combined_st_anomaly_coarse/">(FAPAR)</a></p>
<p align="left"><h6>Tabla 1: Niveles de impacto del CDI</h6></p>
<p align="left"><img src="CDI.esp.png" alt="Combined"> </img></p>
</div>
<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
