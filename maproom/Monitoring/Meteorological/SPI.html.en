<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Standardized Precipitation Index</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="es" href="SPI.html?Set-Language=es" />
<link class="share" rel="canonical" href="SPI.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/SOURCES/.Peru/.UnifiedPrecipitation/.Mask_Peru/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/++//T/666.5/plotvalue/Y/-18.25/-0.25/plotrange+//plotborder+72+psdef//plotaxislength+550+psdef//XOVY+null+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="en">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="dlimg dlimgloc admin share" name="bbox" type="hidden" />
<input class="dlimg dlauximg onlyvar dlimgts share" name="var" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="share dlimgloc dlimgts" name="region" type="hidden" />
<input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="0.5" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoring</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Meteorological Drought</span></legend>
            </fieldset>                  
         <fieldset class="navitem">
            <legend>Region</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsPeru.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Region">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
         </fieldset>

         <fieldset class="navitem">
            
            <legend>Analysis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
               <option value="">1-Month SPI</option>
               <option value="SPI3">3-Month SPI</option>
               <option value="SPI6">6-Month SPI</option>
               <option value="SPI9">9-Month SPI</option>
               <option value="SPI12">12-Month SPI</option></select>
            
         </fieldset> 
         
         
         <fieldset class="navitem">
            
            <legend>Spatially Average Over</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="0.5">gridpoint</option>
               <option value="irids:SOURCES:Features:Political:Peru:Departamento:ds">Department</option>
               <option value="irids:SOURCES:Features:Political:Peru:Provincia:ds">Province</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:AAA:ds">Administrative Water Authority (AAA)</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:ALA:ds">Local Administrative Authority (ALA)</option>
               <option value="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds">Hydrological Unit</option></select>   
   	
            
            <link class="admin" rel="iridl:hasJSON" href="http://iri.ana.gob.pe/expert//labelgeoIdintersects/%7Bndim/0.0/gt/%7B%28bb:-83:-17:-67:0:bb%29geoobject%7Dif/2//labelgeoIdintersect/publicproc:/%7Bageom/ageoobject%7Dinputs/ageom/dup/parent/.dataset/.label/%7Blocation/label%7Dds/location/ageoobject/geometryintersects/0/maskle/SELECT/location/label/1/index/.streamgrids/iridl:geoId/%28:ds%29rsearch/%7Bpop/pop%7Dif/1/index/a:/.datatype//namearraytype/eq/:a:/.name/:a/cvntos/%28:%29exch/append/exch/%7B%28%40%25s:ds%29%7D%7B%28%40%25d:ds%29%7Difelse/append/append/sprintf//name//region/def/use_as_grid/nip/nip/1output/:publicproc%7Ddef/%280.0125%29//resolution/parameter/geoobject/%28bb:-83:-17:-67:0:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Description</a></li>
      <li><a href="#tabs-2" >More information</a></li>
      <li><a href="#tabs-3" >Dataset Documentation</a></li>
      <li><a href="#tabs-4" >Contact Us</a></li>
      <li><a href="#tabs-5" >Instructions</a></li>
    </ul>

         <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable" href="http://www.climatedatalibrary.cl/http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/%28bb:-70%2C-5.0%2C-69%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI%29def//units/%28-%29def/T/exch/table-/text/text/skipanyNaN/-table/.html/" shape="rect"></a>
            
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.Analysis/.MODIS/.Countrymask/.mask_coarse/X/Y/%28bb:-82%2C-19%2C-68%2C1%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+//plotborder+0+psdef//plotaxislength+120+psdef+.gif" />
               
               
            </div>	
            
            
            
            <div style="float: left;">
               	
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://iri.ana.gob.pe/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  <div class="template"> Observations for <span class="bold iridl:long_name"></span>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  <div> SPI values for the current month:</div> 
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI/SPI1/T/last/VALUE/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI1:%20%29def/SPI3/T/last/VALUE/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI3:%20%29def/SPI6/T/last/VALUE/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI6:%20%29def/SPI9/T/last/VALUE/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI9:%20%29def/SPI12/T/last/VALUE/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI12:%20%29def/5/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/SPI_discrete_colors/DATA/-2.5/2.5/RANGE/dup/T/fig-/colorbars2/-fig/+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />  
            
            
         </fieldset> 
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
            
            <link rel="iridl:hasFigure" href="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/SOURCES/.Peru/.UnifiedPrecipitation/.Mask_Peru/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/" />
            <img class="dlimg" src="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/SOURCES/.Peru/.UnifiedPrecipitation/.Mask_Peru/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/++//T/last/plotvalue/Y/-18.25/-0.25/plotrange+//plotborder+72+psdef//plotaxislength+550+psdef//XOVY+null+psdef+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/SOURCES/.Peru/.UnifiedPrecipitation/.Mask_Peru/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/.auxfig+//T/last/plotvalue/Y/-18.25/-0.25/plotrange+//plotborder+72+psdef//plotaxislength+550+psdef//XOVY+null+psdef+.gif" />
            
            
         </fieldset>

 <div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Standardized Precipitation Index</h2>
<p align="left" property="term:description">This map can be used to identify the intensity of drought or excess of rainfall for each month. It shows the Standardized Precipitation Index (SPI).</p>
<p align="left">SPI values more negative than -1 indicate a condition of drought, the more negative the value the more severe the situation. SPI values higher than +1 indicate more humid conditions compared to a normal situation. When the SPI has a value between -1 and +1 the situation is identified as normal. The table below can be used to interpret the SPI value.</p>
<p align="left"><h6>Table 1: Range of Standardized Precipitation Index</h6></p>
<p align="left"><img src="Escala_SPI_eng.jpg" alt="SPI"> </img></p>
<p align="left">Colors more close to red in the map indicate a dry condition, colors more close to green indicate a humid condition compared to the normal condition that specific area.</p>
<p align="left">The SPI is available for different accumulation periods: 1, 3, 6, 9 and 12 months. This allows evaluating the duration of drought or humid condition at different time scales. You can select the time scale in the menu>analysis.
</p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h2  align="center">How is the SPI calculated?</h2>
<p align="left">The Standardized Precipitation Index (SPI; McKee 1993) is the number of standard deviations that observed cumulative precipitation deviates from the climatologically average. It can be calculated for any time scale. Here the SPI is available for 1, 3, 6, 9 or 12 months.</p>
<p align="left">To compute the index, a long-term time series of precipitation accumulations over the desired time scale are used to estimate an appropriate probability density function. The analyses shown here are based on the Pearson Type III distribution (i.e., 3-parameter gamma) as suggested by Guttman (1999). The associated cumulative probability distribution is then estimated and subsequently transformed to a normal distribution. The result is the SPI, which can be interpreted as a probability using the standard normal distribution (i.e., users can expect the SPI to be within one standard deviation of the mean about 68% of the time, two standard deviations about 95% of the time, etc.) The analyses shown here utilize the FORTRAN code made available by Guttman (1999).</p>
<p align="left"><b>References</b> </p>
<p align="left">Guttman, N. B., 1999:  Accepting the Standardized Precipitation Index:  A calculation algorithm. <i>J. Amer. Water Resour. Assoc.</i>., <b>35(2)</b>, 311-322.
<br />McKee, T. B., N. J. Doesken, and J. Kliest, 1993:  The relationship of drought frequency and duration to time scales.  In <i>Proceedings of the 8th Conference of Applied Climatology, 17-22 January, Anaheim, CA</i>.  American Meteorological Society, Boston, MA. 179-184.
 </p>
</div>


<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">Dataset Documentation</h2>
<dl class="datasetdocumentation">
<dt>Data</dt><dd>CPC Unified Precipitation with a spatial resolution of 0.5&#176; lat/lon <a href="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI/">data</a></dd>
<dt>Data Source</dt><dd>NOAA NCEP Climate Prediction Center, <a href="http://www.esrl.noaa.gov/psd/data/gridded/data.unified.html">CPC Unified Precipitation</a></dd>
</dl>
</div>


<div id="tabs-4"  class="ui-tabs-panel">
<h2  align="center">Helpdesk</h2>
<p>
Contact <a href="mailto:mwar-lac@unesco.org?subject=SPI Chile">mwar-lac@unesco.org</a> with any technical questions or problems with this Map Room.
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instructions</h3>
<div class="buttonInstructions"></div>
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
</div>
 </body>
 </html>
