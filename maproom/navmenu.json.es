{
"@context": {
"links": { "@container": "@list" },
"groups": {}
},
"options": [
{"href": "http://www.ana.gob.pe/",
 "title": "Autoridad Nacional del Agua"}
],
"groups": [
{
"title": "Socios",
"links": [
{"href": "http://www.senamhi.gob.pe/",
 "title": "Servicio Nacional de Meteorologia e Hidrologia del Peru"},
{"href": "http://www.igp.gob.pe/",
"title": "Instituto Geofisica del Peru"},
{"href": "http://labtel.fisica.unmsm.edu.pe/",
 "title": "Universidad Nacional Mayor de San Marcos"}
]
},
{
"title": "Desarrollado con apoyo de",
"links": [
{"href": "http://www.unesco.org/new/en/santiago/natural-sciences/hydrological-systems-and-global-change/",
 "title": "UNESCO"},
{"href": "http://www.rlc.fao.org/es/",
 "title": "FAO"},
{"href": "http://iri.columbia.edu/",
 "title": "International Research Institute"},
{"href": "http://www.cazalac.org/",
"title": "Centro del Agua para Zonas Aridas"}
]
}
]
}